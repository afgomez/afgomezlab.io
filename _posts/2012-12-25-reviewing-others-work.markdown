---
layout: post
title: "Reviewing others work"
date: 2013-01-04 16:40
comments: true
categories:
---

Recently we had a junior freelance working with us. After he finished the work we take a look at it
and it was a bit messy. That was expected (he was a junior after all), and we decided it could be
good for everyone to write a review.

I think is the first time in my life I have to do a "formal" review of someone else's work, and
honestly, I have no idea of how to do it. So I asked myself: if someone had to review my code, what
would **I want to hear**?

<!-- more -->

Let's face it: [we all write bad code][wtf]. I wrote really bad code when I started to work as a
developer and I'm probably doing it right now. But I improved because usually someone else noticed
me I was wrong, and what they say to me and how can be summed up in three simple rules:

## 1. If something is wrong, just say it

But don't limit yourself to point to the mistake. Explain **why** is wrong and provide a better
alternative. Is he using `<div>`s for everything? Tell him about the importance of HTML semantics
and propose an alternative. Does he copy and paste too much? Explain how [repeating yourself][dry]
affects the future maintenance of the application and propose a refactored version of the code.

## 2. Separate facts from opinions

Is your way of doing things really better or it's just your opinion of how things should be done?
Maybe the code you are reviewing is not bad, it's simply different.

## 3. Be kind

If you are an asshole people won't listen to you even if you are right, so just be kind.


[wtf]: http://thedailywtf.com/Articles/Guest_Article_0x3a__Our_Dirty_Little_Secret.aspx
[dry]: http://en.wikipedia.org/wiki/Don%27t_repeat_yourself
