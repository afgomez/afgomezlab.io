---
layout: post
title: "Let's be productive"
date: 2012-11-23 23:13
comments: true
categories:
- bash
- productivity
---

I distract myself quite easily.

If the computer is taking longer than five seconds to do something (git push, downloading a big
PSD...) I inmediatly open a browser tab and go to facebook, and then I completely forget what I was
doing. I needed a solution, but I cannot [afford a woman to slap me in the face][1] everytime I
enter a not professional website.

My first idea was to find some firefox addon to block the productivity- blackhole websites, but that
will only work for one browser. I can still open a new terminal tab and use
[links](http://links.twibright.com/)!

So I scratched my own itch and coded a simple bash script that blocked the hosts I wanted in the
whole system. Let's introduce `lets`.

<!-- more -->

{% gist 748896 %}

The idea is really simple. The script redirects the selected hosts to 127.0.0.1 using the
`/etc/hosts` file.

# Installation and usage

Download the gist (read it first to ensure I'm not doing evil things with your files) into a script
and make it executable

    $ sudo curl https://raw.github.com/gist/748896/89d9c3286827a7fe4dce67504b90ea5443ae8c71/lets.sh > /usr/local/bin/lets
    $ sudo chmod +x /usr/local/bin/lets

Then create a file in `/etc/hosts.work` with the hosts. You can add comments if you want.

{% highlight javascript %}
# Add a line for each host you want to block
# You can add multiple hosts in each line

facebook.com
twitter.com
youtube.com
vimeo.com
{% endhighlight %}

And then force yourself to work with this simple command

    $ sudo lets work

Everytime you'll try to access one of the requested pages you'll be redirected to your own machine.
If you have a webserver installed you can create a landing page to remind yourself you should be
doing something else. Once you've finished your day you can unblock the sites with this command.

    $ sudo lets play

And thats all. Be productive!

[1]:http://hackthesystem.com/blog/why-i-hired-a-girl-on-craigslist-to-slap-me-in-the-face-and-why-it-quadrupled-my-productivity/
