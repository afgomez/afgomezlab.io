---
layout: post
title: "A saner reset stylesheet"
date: 2012-12-08 00:01
comments: true
categories:
- CSS
---

A reset stylesheet creates a base where we the developers can build our web pages without worring
about the differences between user agents. There are many reset stylesheets out there, being the
[Eric Meyer][meyers-reset] reset the most famous.

And I don't like it.

<!-- more -->

Don't get me wrong. I think reset stylesheets are needed, but Meyer's one, while useful, has three
fundamental problems.

### 1. It resets too much

It resets `margin`, `padding`, `border`, etc. in every element in the page. Many HTML elements
doesn't have any of those properties by default, so there's no point in resetting them. Also,
personally I've never seen the point in changing the default `font-weight` in `<strong>` elements.

### 2. It adds a lot of noise when debugging

Like, a lot. This is the firebug output of a normal `p` element.

<img src="/images/css-reset/firebug.png" alt="Firebug" class="center" />

Notice the size of the scrollbar. Since the reset applies to every element on the page, every tag
will inherit the resetted styles from each of its ancestors.

### 3. It matches every element in the web page

Since it covers every HTML element the reset will match every existing element in the page, and in
big pages performance can suffer. Since I like to prove my points with data, I searched a page that
used Meyer's reset and profiled the CSS selectors performance.

I compared in github [two Rails branches][rails-branches] and downloaded into my laptop the "Files
Changed" tab. Then I measured the CSS performance using Chrome's CSS profiler. Here's what I saw

<img src="/images/css-reset/github-before.png" alt="Github before" class="center" />

Holy cow! Nearly half a second only to apply the reset rule. That's a lot of time!


## A saner approach

A couple of years ago I created a [base stylesheet][afgomez-reset] to replace the Meyer's one in my
projects. It does many things but fot this post I will focus on the reset part.

{% highlight css %}
body { margin:0; }

/* Text elements */
h1, h2, h3, h4, h5, h6, p, pre, blockquote {
  margin:0; font-size:1em;
}
ul, ol, dl, dt, dd {
  padding:0; margin:0;
}
ul, ol {
  list-style:none;
}
address, cite {
  font-style:normal;
}
/* Embedded elements */
img {
  border:none; -ms-interpolation-mode:bicubic;
}
figure {
  margin:0;
}

/* Fix HTML5 elements in IE */
article, aside, dialog, figure, footer, header, hgroup, nav, section {
  display:block;
}

/* Table elements */
td, th {
  vertical-align:top;
}
caption, th {
  text-align:left;
}

/* Form elements */
form {
  margin:0;
}
fieldset {
  margin:0;
  padding:0;
  border:none;
}
legend {
  margin:0;
  padding:0;
  color:#000;
}
input, textarea, select {
  font:1em Arial,Helvetica,sans-serif;
}
textarea {
  overflow:auto;
}
input[type="text"] {
  margin:0;
}
{% endhighlight %}

Instead of resetting everything it only zeroes `margins` and `paddings` that need to be zeroed and
leaves the user agent defaults when they make sense.

Also, since it applies to a smaller amount of elements it's faster than Meyer's one. Since I downloaded the Github page I was able to change the CSS and test if there where any improvements:

<img src="/images/css-reset/github-after.png" alt="Github after" class="center" />

Not bad. 355ms just for changing the reset stylesheet.

## Final thoughts

Use whatever works for you but investigate all the existing options first, or even create your own.
I'm not saying Meyer's reset is bad but is not perfect and didn't worked for me. My approach isn't
free of problems as well and I'm looking other options like [normalize.css][normalize], or adapting
my existing one to the way I work now.


[meyers-reset]: http://meyerweb.com/eric/tools/css/reset/
[afgomez-reset]: https://github.com/afgomez/base.css
[rails-branches]: https://github.com/rails/rails/compare/master...3-2-stable
[normalize]: http://necolas.github.com/normalize.css/
