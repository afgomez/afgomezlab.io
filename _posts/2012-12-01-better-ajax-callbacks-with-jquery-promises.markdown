---
layout: post
title: "Better AJAX callbacks with jQuery promises"
date: 2012-12-01 18:22
comments: true
categories:
- javascript
- jquery
- ajax
---

Imagine we'll build the next ToDo list manager app and we're going to use javascript and jQuery for
the views. Our backend team has the API already done and we only need to fetch the tasks and render
them into our web page.

Enough talk, Let's do it!

{% highlight javascript %}
function getTasks() {
  $.getJSON( '/api/tasks.json', function( tasks ) {
    // Magical rendering happens here
  });
}
{% endhighlight %}

That's a quite simple function, and simple is good, simple is maintainable. However it has has two
problems.

1. It does two different things. The AJAX call and the rendering.
2. Because of point 1 testing that piece of code is quite difficult. To test the rendering part we
   need to perform an AJAX request, making the test slow.

We want simple, testable parts, and we don't want slow tests, so lets move the rendering part to a
different function.

<!-- more -->

{% highlight javascript %}
function getTasks() {
  $.getJSON( '/api/tasks.json', renderTasks );
}
function renderTasks( tasks ) {
  // Render algorithm. Patent pending
}
{% endhighlight %}

That looks better. Now it's easier to test each piece individually. Good work!

The next feature of our ToDo app is the posibility to assing tasks to users. To do that we're going
to fetch a list of users from the server and put in our page. We now know how to separate the two
actions, so let's do it.

{% highlight javascript %}
function getUsers() {
  $.getJSON( '/api/users.json', renderUsers );
}
function renderUsers( users ) {
  // Append users to the sidebar...
}
{% endhighlight %}

Our app looks much better now, but our product manager has found a problem. The render of the two
elements happens asyncronously and the UI looks weird until everything is loaded. We have to wait
for the two requests to finish before rendering anything. Our best idea is launch the first request,
wait for it to finish, launch the second one and then render everything.

{% highlight javascript %}
function getEverything() {
  $.getJSON( '/api/tasks.json', function( tasks ) {
    $.getJSON( '/api/users.json', function( users ) {
      renderTasks( tasks );
      renderUsers( users );
    });
  });
}
{% endhighlight %}

Well, it's not the prettiest thing in the world, but it works.

Now the UX designer has told us that we should notify the user in case something went wrong. We need
to add an `error` callback for that, and since we cannot do it with `$.getJSON` we need to refactor
our code to use `$.ajax` instead.

{% highlight javascript %}
function getEverything() {

  // Dear future mantainer. Forgive me for my mistakes
  $.ajax( '/api/tasks.json', {
    dataType: 'json',
    success: function( tasks ) {
      $.ajax( '/api/users.json', {
        dataType: 'json',
        success: function( users ) {
          renderTasks( tasks );
          renderUsers( users );
        },
        error: function( jqXHR, textStatus ) {
          notifyError( 'Something went wrong. We are sorry' );
        }
      });
    },
    error: function( jqXHR, textStatus ) {
      notifyError( 'Something went wrong. We are sorry' );
    }
  });
}
{% endhighlight %}

Well... our simple function has evolved into a big mess of anidated callbacks, repeated code and
pain. Have you ever seen something like that and wished there was a better way to do it? There is
it!

## Introducing jQuery.Deferred

jQuery 1.5 introduced the [$.Deferred](http://api.jquery.com/category/deferred-object/) object. A
deferred is and object that holds the status of an operation and calls different callbacks when that
operation is resolved

{% highlight javascript %}
var def = $.Deferred();
def.done( function() { console.log( 'Yay!' ); } );
def.resolve(); // Yay!
{% endhighlight %}

or rejected

{% highlight javascript %}
var def = $.Deferred();
def.fail( function() { console.log( 'Ouch!' ); } );
def.reject(); // Ouch!
{% endhighlight %}

The deferred objects can return a promise, which is just the same object encapsulated in a way that
cannot be resolved/rejected.

{% highlight javascript %}
var def = $.Deferred();
var promise = def.promise();

promise.done( function() { console.log( 'Yay!' ); } );
promise.resolve() // TypeError: promise.resolve is not a function
def.resolve() // Yay!
{% endhighlight %}

Since jQuery 1.5 all the `$.ajax` methods returns a `jqXHR` object, which implements a promise for
the ajax request. That means we can attach the callbacks to that object instead of passing them as
arguments.

This has several beneficts: first, we now can attach several callbacks to the same request. We also
can handle error callbacks in `$.get` and `$.post` methods (I always have found these methods more
readable than `$.ajax`). But the most important thing is that we can pass the `jqXHR` object between
functions.

Let's rewrite our application from sratch using promises. First the tasks feature.

{% highlight javascript %}
function getTasks() {
  return $.getJSON( '/api/tasks.json' );
}
function renderTasks() { ... };

getTasks().done( renderTasks );
{% endhighlight %}

Using promises we managed to completely decouple the retrieving of data from the rendering. We now
can use the getTasks function for other things (we'll get to this later).

Now the same with users.s

{% highlight javascript %}
function getUsers() {
  return $.getJSON( '/api/users.json' );
}
function renderUsers() { ... };

getUsers().done( renderUsers );
{% endhighlight %}

## Handling multiple requests

That was easy, but we still have to fix the multiple request problem. We are going to use another
[jQuery helper](http://api.jquery.com/jQuery.when/) method for that: `$.when`.

What `$.when` does is really easy. It takes several deferred/promises objects and execute callbacks
for all of them following this rules:

* If **all** the deferreds are resolved, execute the `done` callbacks.
* If **one** of the deferres is rejected, execute the `fail` callbacks.

That's pretty awesome. Now we can launch both requests simultaneously and when both are finished we
can render the UI. Knowing all that we can now rewrite our controversial block of code to something
more readable.

{% highlight javascript %}
function getEverything() {
  // Remember: getTasks and getUsers returns a promise
  return $.when( getTasks, getUsers );
}
getEverything.done( function( tasks, users ) {
  renderTasks( tasks[0] );
  renderUsers( users[0] );
}).fail( function() {
  notifyError( 'Something went wrong. We are sorry' );
});
{% endhighlight %}

The arguments for the `done` and `fail` callbacks is a list of the arguments to each deferred
object. In our case, `tasks` will hold the arguments of the ajax response returned by `getTasks`,
and the same for `users` and `getUsers`. In our case this will be the arguments that `$.getJSON`
will normaly pass his callbacks.

{% highlight javascript %}
getEverything.done( function( tasks, users ) {
  tasks; // [json_data, status, jqXHR]
  users; // [json_data, status, jqXHR]
})
{% endhighlight %}

To help with that jQuery has a very convenient method called `pipe()` that allow us to filter the
output of a deferred response. Let's rewrite our fetch functions to use it.

{% highlight javascript %}
function getTasks() {
  return $.getJSON( '/api/tasks.json' ).pipe( function( tasks, status, jqXHR ) { return tasks; } );
}

function getUsers() {
  return $.getJSON( '/api/users.json' ).pipe( function( users, status, jqXHR ) { return users; } );
}
{% endhighlight %}

Now we can use the value directly in our `$.when` callbacks.

{% highlight javascript %}
getEverything.done( function( tasks, users ) {
  renderTasks( tasks );
  renderUsers( users );
});
{% endhighlight %}

Now our code is much more readable. The different parts are less coupled, so they are easier to test
and reutilize in other parts. But the most important thing is we no longer need to ask for
forgiveness to the future mantainer :)
